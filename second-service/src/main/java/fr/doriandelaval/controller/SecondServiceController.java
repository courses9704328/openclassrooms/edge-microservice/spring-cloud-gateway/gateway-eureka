package fr.doriandelaval.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/consumer")
public class SecondServiceController {

    @GetMapping("/message")
    @ResponseStatus(HttpStatus.OK)
    public Mono<String> getMessage() {
        return Mono.just("Hello from second-service");
    }
}
