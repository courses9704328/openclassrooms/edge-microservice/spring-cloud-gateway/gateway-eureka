package fr.doriandelaval.configuration;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringCloudRoutesConfig {

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder routeLocatorBuilder) {

        return routeLocatorBuilder.routes()
                .route("first-service", r -> r.path("/employee/**").uri("lb://FIRST-SERVICE"))
                // use "lb" for load balancer in eureka
                .route("second-service", r -> r.path("/consumer/**").uri("lb://SECOND-SERVICE"))
                .build();
    }
}
