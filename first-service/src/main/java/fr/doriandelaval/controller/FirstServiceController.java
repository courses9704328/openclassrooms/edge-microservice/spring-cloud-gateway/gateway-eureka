package fr.doriandelaval.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/employee")
public class FirstServiceController {
    
    @GetMapping("/message")
    @ResponseStatus(HttpStatus.OK)
    public Mono<String> getMessage(){
        return Mono.just("hello from first-service");
    }
}
